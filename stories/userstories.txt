1. An anonymous User will expect to be able to... 
    1.1 See the main page with app abilities
       1.1.1 See the cities that are listed that have travel hot spots
    1.2 Have the option to sign up for an account so that then can gain logged in user abilities
       1.2.1 login to identify themselves to access other user abilities

2. A Logged in User will expect to be able to...
    2.1 See the main page with app abilities, and select a city that they wish to travel to and explore
        2.1.1 From there, as they reach their goals of seeing these sightseeing opportunities, they can check them off
    2.2 Once the user checks off  certain amount, they are granted access to the next level of the city they are traveling in, 
           gaining access to more in depth ways to travel the city and indulge in the culture
    2.3 They can keep track of their progress in their profile page
        2.3.1 They can keep progress of their check marks from each city, which level they are on, and from there they can unlock
                More cities that are "under the radar" hot spots for traveling. 
                
3. An Administrative User will expect to be able to....
    3.1 do everything an anonymous and a logged in user can do
    3.2 grant incentives to use the app as a travel tool such as "freebies", being a restaurant, a nice place to watch the sunset
        Etc. Depending on how much they are utilizing the app.
    3.3 Update the app as changes come along